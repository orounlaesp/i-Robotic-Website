function Footer() {
    return (
        // <!-- ======= Footer ======= -->
        <>
        <footer id="footer" className="footer">

            <div className="footer-content">
                <div className="container">
                    <div className="row">

                        <div className="col-lg-3 col-md-6">
                            <div className="footer-info">
                                <h3>I-Robitics</h3>
                                <p>
                                    Fidjrossè<br/>
                                        Cotonou, Bénin<br/><br/>
                                            <strong>Tél.:</strong> +229 99 29 81 81<br/>
                                                <strong>E-mail:</strong> support@irobitics.store<br/>
                                                </p>
                                            </div>
                                        </div>

                                            <div className="col-lg-2 col-md-6 footer-links">
                                                <h4>Liens utiles</h4>
                                                <ul>
                                                    <li><i className="bi bi-chevron-right"></i> <a href="/about">Qui sommes-nous ?</a></li>
                                                    <li><i className="bi bi-chevron-right"></i> <a href="/services">Que faisons-nous ?</a></li>
                                                    <li><i className="bi bi-chevron-right"></i> <a href="/blog">Nos derniers articles</a></li>
                                                    <li><i className="bi bi-chevron-right"></i> <a href="/contact">Prendre Contact</a></li>
                                                    <li><i className="bi bi-chevron-right"></i> <a href="#">Conditions d&apos;utilisation</a></li>
                                                    <li><i className="bi bi-chevron-right"></i> <a href="#">Politique de confidentialité</a></li>
                                                </ul>
                                            </div>

                                            <div className="col-lg-3 col-md-6 footer-links">
                                                <h4>Nos Services</h4>
                                                <ul>
                                                    <li><i className="bi bi-chevron-right"></i> <a href="/services">Fabrication de drones</a></li>
                                                    <li><i className="bi bi-chevron-right"></i> <a href="/services">Logiciels d&apos;imagerie</a></li>
                                                    <li><i className="bi bi-chevron-right"></i> <a href="/services">Cartographie numérique</a></li>
                                                    <li><i className="bi bi-chevron-right"></i> <a href="/services">Analyse géospatiale</a></li>
                                                    <li><i className="bi bi-chevron-right"></i> <a href="/services">Visualisation de données</a></li>
                                                    <li><i className="bi bi-chevron-right"></i> <a href="/services">Géodésie</a></li>
                                                    <li><i className="bi bi-chevron-right"></i> <a href="/services">Topographie</a></li>
                                                    <li><i className="bi bi-chevron-right"></i> <a href="/services">Les inspections</a></li>
                                                </ul>
                                            </div>

                                            <div className="col-lg-4 col-md-6 footer-newsletter">
                                                <h4>Notre Newsletter</h4>
                                                <p>Ne manquez plus aucune information, abonnez-vous pour recevoir nos nouvelles offres en premier.</p>
                                                <form action="" method="post">
                                                    <input type="email" name="email"/><input type="submit" value="Je m'abonne"/>
                                                    </form>

                                                    </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div className="footer-legal text-center">
                                        <div className="container d-flex flex-column flex-lg-row justify-content-center justify-content-lg-between align-items-center">

                                            <div className="d-flex flex-column align-items-center align-items-lg-start">
                                                <div className="copyright">
                                                    &copy; Copyright <strong><span>I-Robotics</span></strong>. All Rights Reserved
                                                </div>
                                                <div className="credits">
                                                    Designed by <a href="https://labourd.tech">Labourd</a>
                                                </div>
                                            </div>

                                            <div className="social-links order-first order-lg-last mb-3 mb-lg-0">
                                                <a href="#" className="twitter"><i className="bi bi-twitter"></i></a>
                                                <a href="#" className="facebook"><i className="bi bi-facebook"></i></a>
                                                <a href="#" className="instagram"><i className="bi bi-instagram"></i></a>
                                                <a href="#" className="google-plus"><i className="bi bi-skype"></i></a>
                                                <a href="#" className="linkedin"><i className="bi bi-linkedin"></i></a>
                                            </div>

                                        </div>
                                    </div>

                                </footer>
                                

                                <a href="#" className="scroll-top d-flex align-items-center justify-content-center"><i className="bi bi-arrow-up-short"></i></a>
                                </>
                                );
}

                                export default Footer;